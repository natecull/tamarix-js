
"use strict";

/*

Tamarix 0.1 by Nate Cull - 15 August 2020

This software is licensed under the MIT License.

tamarix-js is a Node.js parser for Term-Expressions.

Term-Expressions are a simple universal structured data format based on combining Prolog Terms with Lisp S-Expressions, giving linked lists of tagged arrays.

A Term-Expression is a triple of:

* A head, like a Prolog term's functor, an object's class/prototype, or an abstract syntax tree's type. 
* An array body, like a Lisp list or a Prolog term body, but with constant access time and contiguous storage.
* A tail, like a Lisp S-Expression's CDR, representing the continuation of the expression beyond the array.

and is represented in Javascript internal storage as a 1, 2 or 3-element array, with a simple Unicode serialisation of trees of these array structures.

Term-Expressions are an attempt at a universal syntax and semantics layer which can be used to integrate all of the following types of data:

* row, column or object-structured databases
* text documents
* wiki, blog and social media posts
* configuration files
* literate programming
* program source code
* program object code
* domain specific languages
* raw runtime memory allocations

For a rough comparison of the kind of thing Term-Expressions are aimed at improving: see S-expressions, XML, Markdown, YAML, LaTeX. 

Tamarix is the Latin genus of the tamarisk or salt cedar, a hardy desert-dwelling tree. https://en.wikipedia.org/wiki/Tamarix




STRING SERIALIZATION SYNTAX

Term-expressions use only three syntax characters: Left [, Right ], and Mark `, plus whitespace for token separation.
Whitespace can be optionally parsed using the reserved `~~~ head.
String literals are represented using quotation sequences starting of [`], [``], [```] etc for open and close quotation.


ARRAY EXPRESSIONS

Term-expressions are parsed by Tamarix from UTF-8 serialization into Javascript arrays, each generally a triple of [Head,Body,Tail].
Where possible, these arrays are then simplified according to the following rules:

* ONLY TAIL: an array with Head and Body both nil becomes just its Tail
* SIMPLE LIST: an array with Head and Tail both nil becomes a one-element array of the Body
* ATOMIC HEAD: an array with Tail and Body both nil, and the Head is a string/number, becomes a one-element array of the Head
* SIMPLE TERM: if the Head and Body are not nil but the Tail is nil, then becomes a two-element array
* ATOMIC BODY: if the Head is not nil, the Body has only one entry, and that entry is a string/number, then the Body is just that entry

Tamarix:


TODO: improve tests

DONE: immutable shared nil object
DONE: head quotes
DONE: writing Javascript arrays
DONE: whitespace parsing
DONE basic writing with string quoting
DONE: magic value for whitespace head in data
DONE: simplify whiteblock
DONE: strict writing with escaping of strings and whitespace
DONE: quote sequences
DONE: parsing complete
DONE: convert to state object
DONE: rename to Tamarix



*/



const tx = {}
module.exports = tx

// An immutable nil object
tx.nil = []
Object.freeze(tx.nil)

tx.chars = {}
tx.chars.mark = '`'
tx.chars.left = '['
tx.chars.right = ']'
tx.chars.space = ' '

// Characters are escapable if they are within ranges that are considered unprintable 
// Escapable characters will be escaped if written in strict mode

tx.escapesets = {}

tx.escapesets.literal = []
tx.escapesets.ASCII = [[0,31]]

tx.escapesets.default = tx.escapesets.ASCII


tx.charIsLinebreak = c => c == '\n'
tx.charIsWhitespace = c => c == ' ' || c == '\t' || c == '\n' || c == '\r' 
tx.charIsReserved = c => c == tx.chars.mark || c == tx.chars.left || c == tx.chars.right
tx.charIsWordlike = c => ! tx.charIsReserved(c) && ! tx.charIsWhitespace(c)

tx.charIsEscapable = (c,escapeset=tx.escapesets.default) => {
	const ccode = c.charCodeAt(0)
	return escapeset.some(r => ccode >= r[0] && ccode <= r[1])


// These are special reserved term heads used in Javascript data structures

tx.magic = {}

// the magic whitespace head is just a unique symbol, it will never be called as a function
// a named function expression is fine for this because it will display as [Function: whitespace]
// if we examine the raw Javascript data

tx.magic.whitespace = function whitespace() {}

}
// These are special reserved term heads used when parsing and writing

tx.heads = {}

// the whitespace block marker indicates a term which contains whitespace parsing, when in literal writing mode
tx.heads.whiteblock = '~~~'

// the normal whitespace head is used for writing whitespace in strict mode
tx.heads.whitespace = '~w'


// Booleans and other logic values

tx.heads.true = '~true'
tx.heads.false = '~false'
tx.heads.null = '~null'
tx.heads.undefined = '~undefined'

// Special object types

tx.heads.string = '~s'
tx.heads.number = '~n'
tx.heads.array = '~a'
tx.heads.object = '~o'

// Head quoting (not the same as quotation sequences for strings)
// This is to allow the reserved heads to still appear in Javascript data, if that's important
// Any of these reserved heads will be quoted with the quote head when written and unquoted when read

tx.heads.quote = '~q'


// Special expressions

// tx.heads.dot = '~dot'


tx.escapes = {
	" ":"_",
	"\n":"n",
	"\r":"r",
	"\t":"t"
}

// unescapes is the opposite of escapes
tx.unescapes = {}
for (const key in tx.escapes) { 
	const value = tx.escapes[key]
	tx.unescapes[value]=key
}

// escaping a character
tx.escapeChar = c => {
	// look up in the escapes table
	const e = tx.escapes[c]
	// if it's a known escape, return that
	if (e) { return e }
	// otherwise return a unicode escape
	return "u" + c.charCodeAt(0).toString(16)
}

// unescaping a character

tx.unescapeChar = e => {
	// If the escape starts with 'u', parse the rest as hex unicode (will default to \x00 if can't be parsed)
	if (e[0] == "u") { return String.fromCharCode(Number.parseInt(e.slice(1),16)) }
	// otherwise try a known unescape
	const c = tx.unescapes[e]
	if (c) { return c }
	
	// if all else fails, return NUL
	return "\x00"
}

// Return an array of escapes for whitespace characters
// Escaping whitespace is much simpler than escaping general strings - we can escape spaces as well

tx.escapeWhitespace = s => [...s].map(tx.escapeChar)

// Unescaping whitespace is simple too

tx.unescapeWhitespace = e => e.map(tx.unescapeChar).join('')


// General string escape
// Convert a string to list of expressions, either:
// strings for sequences of nonescapable characters
// TX lists for sequences of escapable characters

tx.escapeString = (s,escapeset) => {
	var e = []
	var str = ""
	var esc = []
	
	// We probably want to handle Unicode correctly so let's do it the hard way by splitting into an array of characters
	
	for (const c of s) {
		if (tx.charIsEscapable(c,escapeset)) {
			// escapable character - maintain esc
			// push the string if needed
			if (str.length > 0) { 
				e.push(str)
				str = ""
			}
			esc.push(tx.escapeChar(c))
		} else {
			// normal character - maintain str
			// push the escape list if needed
			if (esc.length > 0) {
					e.push([esc])
					esc = []
			}
			str += c
		}
	}
	
	// push any remaining string or escape
	if (str.length >0 ) { e.push(str) }
	if (esc.length >0) {e.push([esc]) }
	
	return e
}

// convert a string escape (a list of strings or character escapes) into a string

tx.unescapeString = e => {

	var s = ""
	for (const x of e) {
		if (tx.isString(x)) { s += x }
		if (tx.isList(x)) { 
			for (const esc of x[0]) {
				s += tx.unescapeChar(esc)
			}
		}
	}
	
	return s
}

tx.verbose = false

tx.log = function(msg) { if (tx.verbose) { console.log(msg) } }

// look up a token in the token dictionary or add it if not found
// this is to force strings to be exactly equal
// also do whatever processing we might need to do for unquoted non-word entities
// like numbers or the term marker



// A numberlike number is one that can be written safely to string and parsed as a number

tx.numberIsNumberlike = xp => tx.isNumber(xp) && ! (isNaN(xp) || xp == Infinity || xp == -Infinity)

// A numberlike string is one that will be parsed as a number (so needs to be quoted if written)

tx.stringIsNumberlike = xp => { 
	if (! tx.isString(xp)) { return false }
	const n = Number(xp)
	if (! tx.numberIsNumberlike(n)) { return false }
	if (n.toString() != xp) { return false}
	return true
}

tx.tokens = {}
tx.maxTokenLength = 999

tx.tokenize = function(w) {

	// force w to be a string if it's not
	
	if (! tx.isString(w)) { w = w.toString() }
	

	
	// If it parses as a Javascript number, and writes as itself, return a number
	if (tx.stringIsNumberlike(w)) { return Number(w) }

	
	// If w is longer than the maximum string size, don't intern it
	// The token cache is really just there as an optimization, not even sure if we actually need it
	
	if (w.length > tx.maxTokenLength) { return w }
	
	// look up the word in the token cache
	
	let tw = tx.tokens[w]
	
	// if it's a known token, return that
	
	if (tw !== undefined) { return tw }

	// otherwise add it to the token cache and return it

	tx.tokens[w] = w
	return w
	
}


// Create a new parse state
// A parse state shows exactly where we're at in parsing

tx.newParseState = flags => {
	const state = { 
			word: "",			// the word being built
			whitespace: "",	// the whitespace being built
			quoting: false,	// are we in quote mode?
			quoteseq: "",		// a quote sequence being built or finished
			wslevel: 0,		// whitespace nesting level - 0 if not in whitespace parsing
			stack: [],			// stack of expressions being built
			head: [],			// head of current expression
			body: [],			// body of current expression
			tail: [],			// tail of current expression
			stage: 0,			// stage of current expression (0 = empty, 1 = awaiting head, 2 = awaiting body, 3 = awaiting tail)
			source: "",		// source of string eg filename, for error reporting
			x: 0,				// character in current line for error reporting
			y: 0				// current line for error reporting
	}
	
	// Not sure what parse flags we need but if there are any, override them here
	
	return state
	
}

// Parse a whole string

tx.parse = (s,flags) => {
	
	// Create a new parse state

	const state = tx.newParseState(flags)
	
	
	// Parse an initial left bracket and space
	// this should open a new expression but not start a quoting sequence
	
	tx.parseChunk(tx.chars.left + tx.chars.space, state)
	
	// Parse the string itself
	tx.parseChunk(s,state)
	
	// If we haven't closed any quotes by now, that's an error
	
	if (state.quoting) { 
		throw {error: "UnclosedQuote",state}
	}
	
	if (state.quoteseq.length > 0) {
			throw {error: "UnclosedQuoteseq",state}
	}
	
	// Parse a final right bracket
	
	tx.parseChunk(tx.chars.right,state)
	
	// If there are unclosed expressions, that's an error
	
	if (state.stack.length > 1) {
		throw {error:"UnclosedBracket", state}
	}
	
	// Return the top (and only) expression on the stack as our expression
	
	return state.body[0]
}


// parseChunk and parseChar update state rather than returning a value
// the parsed values, end up on the stack (or in the word/whitespace registers)

tx.parseChunk = (s,state) => {
	for (const c of s) { tx.parseChar(c,state) }
	
}	

tx.parseChar = (c,state) => {
	// Update coordinates
	if (tx.charIsLinebreak(c)) {
		state.y++
		state.x = 0
	} else {
		state.x++
	}
		
	
	// Are we in quoting mode?
	if (state.quoting) {
		// add character to the word we're building
		state.word +=c
		
		// Have we matched the end-quote sequence?
		const quotelen = state.quoteseq.length
		if (state.word.slice(-quotelen) == state.quoteseq) {
				// get the word without the quote sequence
				const actualWord = state.word.slice(0,state.word.length -quotelen)
				// add it to our expression
				tx.addExp(actualWord,state)
				// turn off quoting mode
				state.word = ""
				state.quoting = false
				state.quoteseq = ""
				return
		}
		
		// Nothing else to do if we're in quoting mode
		return
	}
	


	
	// If it's nonwhitespace, and we have pending whitespace, and we care about whitespace, process that now
	
	if (! tx.charIsWhitespace(c) && state.whitespace.length > 0) {
		
		// Are we tracking whitespace? And is it anything other than one space?
		if (state.wslevel > 0 && state.whitespace != tx.chars.space) { 
			// Do either a really dumb or really smart thing and parse the whitespace as an expression
			
			// Push a new whitespace tail
			state.stage= 3
			tx.pushExp(state)
			state.head = tx.heads.wsmagic
			state.body = tx.escapeWhitespace(state.whitespace) 
			
			// Push a new empty expression as tail
			state.stage = 3
			tx.pushExp(state)
			
		} 
		
		// Clear pending whitespace
		state.whitespace = ""
	}
	
	// If it's not a wordlike character, then terminate a word if we have one
	
	if (! tx.charIsWordlike(c) && state.word.length > 0) {
		tx.addExp(tx.tokenize(state.word),state)
		state.word = ""
	}
	
	
	// Are we building a quote sequence?
	// Second and subsequent characters must be mark
	if (c == tx.chars.mark && state.quoteseq.length > 0) {
		state.quoteseq += c
		return
	// Final char must be right, so turn on quoting
	} else if (c == tx.chars.right && state.quoteseq.length >1) {
		state.quoteseq += c
		state.quoting = true
		return
	// Anything but a mark or a right once a quoteseq is [`` or greater is an error
	} else if (state.quoteseq.length >2) {
		throw {error:"BadQuoteseq",state}
	}


	// Ok so we're not quoting and we're not building a quote sequence

	
	// Handle any partial quote sequence we were starting to build
	
	
	if (state.quoteseq == tx.chars.left) {
		// Push a new list
		state.quoteseq = ""
		tx.pushExp(state)
	}
	
	if (state.quoteseq == tx.chars.left + tx.chars.mark) {
		// Push a new term
		state.quoteseq = ""
		tx.pushExp(state)
		state.stage=1
	}
	
	// Otherwise handle the character itself
	
	
	// Left
	
	if (c == tx.chars.left) {
		// create a new potential quoteseq
		state.quoteseq = c
		return
	}
	
	// Mark
	
	if (c == tx.chars.mark) {
		// Mark or continue this exp as a term
		
		// Two marks in a row, if not in a quote sequence, are an error
		if (state.stage == 1) {
			throw {error: "BadQuoteseq",state}
		}
		
		// If the expression is empty, turn it into a term
		if (state.stage ==0) {
			state.stage = 1
			return
		}
		
		// Otherwise, push a new term as a tail
		
		state.stage = 3
		tx.pushExp(state)
		state.stage=1
		
	}
	
	// Right
	
	if (c == tx.chars.right) {
		// A right after a mark is an error (because it could be mistaken for a quoteseq)
		
		if (state.stage == 1) {
			throw {error:"BadQuoteseq",state}
		}
		
		
		// Pop expressions
		
		do {
			tx.popExp(state)
			
		} while (state.stage == 3)
	}

	// If it's whitespace, add it to the whitespace string
	
	if (tx.charIsWhitespace(c) && state.wslevel > 0) {
		state.whitespace += c
		return
	}
	
	// If it's a wordlike character, add it to the word string
	
	if (tx.charIsWordlike(c)) {
		// Just add it to the word we're building
		state.word += c
		return
	}
	
}


// add a subexpression to an expression

tx.addExp = (xp,state) => {
	if (state.stage == 1) {
		// stage 1 means we're setting the head of an expression
		state.head = xp
		state.stage = 2
		
		// Warning this is a little magic
		// Detect whitespace block markers
		if (xp == tx.heads.whiteblock && state.wslevel == 0) { 
				state.wslevel = 1 
		}
		return
	}
	if (state.stage == 0) {
		// stage 0 means this was an empty list we're appending the first element to
		state.body.push(xp)
		state.stage = 2
		return
	}

	if (state.stage == 2) {
		// stage 2 means we're appending to the body of an expression
		state.body.push(xp)
		return
	}
	
	if (state.stage == 3) {
		// stage 2 means we're setting the tail of an expression
		// this will only be used when popping an expression
		state.tail = xp
		return
	}
}

// push this expression to the stack

tx.pushExp = state => {
	state.stack.push([state.stage,state.head,state.body,state.tail])
	state.stage = 0
	state.head = []
	state.body = []
	state.tail = []
	
	// Update whitespace level
	if (state.wslevel >0) { 
		state.wslevel ++
	}
}

// Pop an expression from the stack

tx.popExp = state => {
	if (state.stack.length == 0) {
		throw {error:"ExtraRightBracket",state}
	}
	
	const newexp = tx.simplifyExp([state.head,state.body,state.tail])
	
	const exp = state.stack.pop()
	
	state.stage = exp[0]
	state.head = exp[1]
	state.body = exp[2]
	state.tail = exp[3]
	
	tx.addExp(newexp,state)
	
	// Update whitespace level
	if (state.wslevel > 0) { 
		state.wslevel-- 
	}
	
}


// Simplify an expression while reading

tx.simplifyExp = xp => {
	
	// Strings, numbers, booleans are themselves
	
	if (tx.isAtomic(xp)) { return xp }
	
	// We should only be dealing with a form at this point
	
	if (! tx.isForm(xp)) { throw {error:"SimplifyUnknownExpression", xp} }
	
	const head = tx.head(xp)
	const body = tx.body(xp)
	const tail = tx.tail(xp)
	
	// Simplify special reserved heads to special Javascript objects
	
	// Whitespace block markers become a list
	if (head == tx.heads.whiteblock) { return tx.simplifyForm([ tx.nil, body,tail ]) }
	
	/*
	// Dot expressions become their first body element
	// TODO - we don't need dot expressions anymore
	
	
	
	if (head == tx.heads.dot) { return tx.simplifyExp(tx.body(xp)[0]) }
	
	*/
	
	// Numbers
	
	if (head == tx.heads.number) { return Number(body[0]) }
	
	// Strings
	
	if (head == tx.heads.string) { return tx.unescapeString(body) }
	
	// Booleans
	
	if (head == tx.heads.true) { return true }
	if (head == tx.heads.false) { return false }
	if (head == tx.heads.null) { return null }
	if (head == tx.heads.undefined) { return undefined }
	
	// Objects
	
	if (head == tx.heads.object) { return tx.simplifyObject(xp) }
	
	// Quotes
	
	if (head == tx.heads.quote) { return tx.simplifyForm([body[0],tx.body(tail),tx.tail(tail)]) }
	
	// Javascript arrays
	
	if (head == tx.heads.array) { return body }
	
	return tx.simplifyForm(xp)
	
	
}

// an object term becomes a Javascript object

tx.simplifyObject = xp => {

		let props = tx.body(xp)
		let i = 0
		let key = null
		let value = null
		let obj = {}
		let error = null

		for (i=0;i<props.length;i+=2) {
			key = props[i]
			if (! tx.isAtomic(key)) { throw {error:"InvalidObjectKey", value: key, object: xp} }
			value = props[i+1]
			obj[key]=value
		}
		
		return obj
}



// Simplify an array-encoded T-expression into its most compact form
		
tx.simplifyForm = xp => {


		// copy the expression into variables

		var head = xp[0]
		var body = xp[1]
		var tail = xp[2]

		// eliminate nulls in case we got a half-built expression somehow
	
		if (head === null  || head === undefined) { head = tx.nil }
		if (tail === null || tail === undefined) { tail = tx.nil }
		
		// ATOMIC BODY [ head, [first], tail ] => [ head, first, tail] if head is not nil
		// a little bit scary but makes for much cleaner looking arrays for short expressions
		
		if (! tx.isNil(head) && tx.isSingleton(body) && tx.isAtomic(body[0])) { 
			tx.log('simplify: atomic body')
			body = body[0] 
		}

		// NIL [ [],[],[] ] ==> [] 
		// not needed as it's covered by ONLY TAIL 

		// if (tx.isNil(head) && tx.isNil(body) && tx.isNil(tail)) { return [] }

		// ONLY TAIL [ [],[],tail ] ==> tail
		// if you have a list with no body, logically the tail MUST be the only thing represented by that form

		if (tx.isNil(head) && tx.isNil(body)) { 
			tx.log('simplify: only tail')
			return tail 
		}

		// SIMPLE LIST [ [],body,[] ] ==> [body]
		// in the specific case of a list with no tail, just wrap it in an array, nice and neat
		// this is a very common case

		if (tx.isNil(head) && tx.isNil(tail)) { 
			tx.log('simplify: simple list')
			return [body] 
		}

		// ATOMIC HEAD  [ head,[],[] ] ==> [head] if head is atomic
		// conversely, if you just have an atom as a head of a term, put that in a one-element array
		// this is also a very common case for symbols

		if (tx.isNil(body) && tx.isNil(tail) && tx.isAtomic(head)) { 
			tx.log('simplify: atomic head')
			return [head] 
		}

		// SIMPLE TERM [ head,body,[] ] ==> [head,body]
		// if you have a more complex term but tail is still nil, you can just drop the tail
		// if the body was also reduced by ATOMIC BODY you might have just a two-atom array  

		if (tx.isNil(tail)) { 
			tx.log('simplify: simple term')
			return [head,body] 
		}

		// otherwise you're going to need all three parts

		return [head,body,tail]
}


// Create a new write state

tx.newWriteState = flags => {
		
	
		const state = {}
		
		// We only want to import known values from flags into state
		
		if (flags == undefined) { flags = {} }
		
		if (flags.strict) { state.strict = true }
		if (flags.escapeset) { state.escapeset = flags.escapeset }
		if (flags.jsarrays) { state.jsarrays = true }
		
		// If we're writing in literal mode, we need to track whitespace level
		
		if (! state.strict) { state.wslevel = 0 }
		
		return state
}

// Write an expression

tx.write = (xp, isTail, state) => {
	// Initialise a write state if we don't have one
	// TODO: we need an api like parse() which sets state for us
	
	if (state == undefined) { state = tx.newWriteState() }
	
	// Atomic values
	
	if (tx.isString(xp))	{ return tx.writeString(xp,isTail,state) }
	if (tx.isNumber(xp))	{ return tx.writeNumber(xp,isTail) }
	if (tx.isBoolean(xp))	{ return tx.writeBoolean(xp,isTail,state) }
	
	// Structured values
	
	if (tx.isNil(xp))		{ return tx.writeWrapped("",isTail) }
	if (tx.isObject(xp))	{ return tx.writeObject(xp,isTail,state) }
	
	if (! tx.isForm(xp)) 	{throw {error:"UnwriteableValue", value: xp, state}  }
	
	const head = tx.head(xp)
	
	// Quotes
	
	if (tx.heads[head]) { return tx.writeHeadquote(xp,isTail,state) }
	
	// Whitespace
	
	if (head == tx.heads.wsmagic) { return tx.writeWhitespace(xp,isTail,state) }
	
	// Javascript arrays
	
	if (state.jsarrays) { return tx.writeArray(xp,isTail,state) }
	
	// Otherwise just write a normal Term-Expression form
	
	return tx.writeForm(xp,isTail,state) 

	
	
}

// Quotes

tx.writeHeadquote = (xp,isTail,state) => tx.write([tx.heads.quote,tx.head(xp),[ tx.nil, tx.body(xp), tx.tail(xp) ] ])

// Write a Boolean (actually a logic value including more than Booleans)

tx.writeBoolean = (b,isTail,state) => {
	if (b === true) { return tx.write([tx.heads.true],isTail,state) }
	if (b === false) { return tx.write([tx.heads.false],isTail,state) }
	if (b === null) { return tx.write([tx.heads.null],isTail,state) }
	if (b === undefined) { return tx.write([tx.heads.undefined],isTail,state) }

	throw {error:"NotBoolean",value:b}
}

// Write a string, wrapped in brackets if not in tail position

tx.writeWrapped = (s, isTail) => {
	var wrapped = ""
	if (! isTail) { wrapped += tx.chars.left }
	wrapped += s
	if (! isTail) { wrapped += tx.chars.right }
	return wrapped
}

// Write a term given that we know the head and a string representation of the rest of it

tx.writeRawTerm = (head, s, isTail) => tx.writeWrapped( tx.chars.mark + head + tx.chars.space + s, isTail)

// Write a number

tx.writeNumber = (n,isTail) => {
		// If the number won't parse like a number, put it inside a number term
		if (! tx.numberIsNumberlike(n)) { 
			return tx.writeRawTerm(tx.heads.number, n.toString(),isTail) 
		} 
		// otherwise just write its string representation	
		// in tail position, write as a dot expression
		
		if (! isTail) { return n.toString() }
		
		return tx.writeRawTerm(tx.heads.number, n.toString(),true) 
		
}


// Write a string

tx.writeString = (s,isTail,state) => {
	
	// Find out if we need to escape the string
	// which will only be if we have 'strict' mode enabled in state
	// and use an escapable range from state or the default one
	
	var escapable
	var escapeset
	if (state.strict) {
		if (state.escapeset) {
			escapeset = state.escapeset 
		} else {
			escapeset = tx.escapesets.default
		}
		escapable = tx.stringNeedsEscaping(s,escapeset)
	} else {
		escapable = false
	}
	
	// If we have to escape, write a string term
	
	if (escapable) { return tx.write([tx.heads.string,tx.escapeString(s)],isTail) }
	
	// If we're not in tail position, write the string, quoted if necessary
	
	if (! isTail) { return tx.quoteString(s) }

	// Otherwise write a string expression with just the string

	return tx.writeRawTerm(tx.heads.string, tx.quoteString(s),true)

	
}

// Does this string contain any nonwordlike characters that need quoting?

tx.stringIsWordlike = s => [...s].every(c => tx.charIsWordlike(c))


// Does this string need quoting?

tx.stringNeedsQuoting = s => ! tx.stringIsWordlike(s) || tx.stringIsNumberlike(s)

// Does this string need escaping?

tx.stringNeedsEscaping = (s, escapeset) => [...s].some(c => tx.charIsEscapable(c,escapeset))



// Quote a string, if needed, using a quote sequence

tx.maxQuoteDepth = 999
tx.quoteString = s => {
	
	// Don't bother quoting if there's no need for it
	
	if (! tx.stringNeedsQuoting(s)) { return s }
	
	var i =0
	var quoteseq = "" 
	
	// Find the smallest quote sequence not contained inside the word to be quoted
	for (i=1;i<tx.maxQuoteDepth;i++) {
		quoteseq = tx.chars.left + tx.chars.mark.repeat(i) + tx.chars.right
		if (! s.includes(quoteseq)) { break }
	}
	if (quoteseq == "") {
			throw {error: "UnquotableString", string: s}
	}
	
	return quoteseq + s + quoteseq
}

// Write a form as a Javascript array

tx.writeArray = (xp,isTail,state) => tx.writeForm([tx.heads.array,xp],isTail,state)

// Write a form, eg, a standard non-atomic, non-special T-expression

tx.writeForm = (xp,isTail,state) => {


	let s = ''
	let head = tx.head(xp)
	let body = tx.body(xp)
	let tail = tx.tail(xp)
	
	// If we're tracking whitespace levels, increase it
	
	if (state.wslevel && ! isTail) { state.wslevel++ }
	
	// wrap the form in left bracket
	
	if (! isTail) { s += tx.chars.left }
	
	// Only write the head if it's not a list
	if (! tx.isNil(head)) { 
		s += tx.chars.mark + tx.write(head,false,state)
		// and a trailing space, if needed
		if (! tx.isNil(body) || ! tx.isNil(tail)) { 
			s += tx.chars.space
		}
		
	}

	// Write the body contents, if any
	if (! tx.isNil(body))	{ 
		s += body.map(x => tx.write(x,false,state)).join(tx.chars.space)
		// and a trailing space, if needed
		if (! tx.isNil(tail)) { 
			s += tx.chars.space
		}
	}
	
	// Write a nil head if the tail is a list
	
	if (tx.isList(tail)) { s += tx.chars.mark + '[]' + tx.chars.space }
	
	// Write the tail, if any
	

	s += tx.write(tail,true,state)
	
	// wrap the form in right bracket
	
	if (! isTail) { s += tx.chars.right }
	
	// If we're tracking whitespace levels, decrease it
	
	if (state.wslevel) { state.wslevel-- }
	
	return s

}

/*
// write a dot-expression given a string 

tx.writeDot = (s, isTail) => isTail ? tx.chars.mark + tx.heads.dot + tx.chars.space + s : s
*/

// write a Javascript object

tx.writeObject = (o,isTail,state) => {
		let oa = []
		Object.entries(o).forEach( ([key,value]) => {
			oa.push(key)
			oa.push(value)
		})
		return tx.write([tx.heads.object, oa],isTail,state)
}


// write whitespace - TODO



tx.writeWhitespace = (xp,isTail,state) => {
	
	// For strict mode, just write out the expression as a whitespace head
	
	if (state.strict) { return tx.write([tx.heads.whitespace,tx.body(xp),tx.tail(xp)],isTail,state) }
	
	// For literal mode: add a whitespace block marker, write the unescaped whitespace, write the tail
	
	var s = ""

	
	if (! state.wslevel) { 
		state.wslevel = 1
		s += tx.chars.mark + tx.heads.whiteblock + tx.chars.space
	}
	
	s += tx.unescapeWhitespace(tx.body(xp))
	
	return tx.writeWrapped(s + tx.write(tx.tail(xp), true,state),isTail)
	
	
}







// Find a list of shared objects referenced more than once by a Javascript object

tx.findSharedObjects = xp => {
// Pre-scan the collection of objects, if it is a collection
	
	let uniques = []
	tx.scanObjects(xp,uniques)
	
	
	// Reduce the list of unique objects to just those referenced more than once
	// Then copy that list with the ref count set to zero
	// This shared list will be updated as we write to increment reference counts
	// The objects will be replaced by their index in the shared list
	
	return uniques.filter(x => x[1] >1).map(x => [x[0],0])
}

// Scan an array of objects finding unique objects and how many times they are referenced 
// This function mutates the array 'uniques' (an array of object, refcount pairs) to add objects and increment counts

tx.scanObjects = (objs, uniques) => {
	
	// Don't do anything if it's not an object or array
	
	if (! tx.isObjectlike) { return } 
	
	// Using a for loop here so we can log the scanned object number for debugging
	
	for (let j=0; j<objs.length;j++) {
		
		const o = objs[j] 
		
		// Check if the object is already in our list of uniques
		const i = uniques.findIndex( x => x[0] === o)
		if (i <0) { 
			
			// if it's not there, add to uniques with a count of 1
			uniques.push([o,1])
			tx.log("scanObjects: obj "+j+ " unique=" + (uniques.length -1))
		} else {
			// otherwise just increase the count
			uniques[i][1] = uniques[i][1] +1
			
			tx.log("scanObjects: obj "+j+" unique=" + i +" count=" + uniques[i][1])
		}
		
		// If it's an object or array - and this is the first time we've seen it - also scan all its children
		// Object.values() will work on both objects and arrays
		// This occurs check should break all cycles and give us a list of unique objects we can serialize
		
		if ( (tx.isObjectlike(o)) && i < 0 )	{ tx.scanObjects(Object.values(o),uniques) }
		
	}
	
	// no return value because we modify uniques and count
}



// method-like functions

// head method accesses the head of a xp, reversing simplifications

tx.head = xp => {
	if (! Array.isArray(xp)) { return undefined }

	// head of [] is [] 

	if (xp.length == 0 ) { return xp }

	// if it's a list, then the head is nil

	if (xp.length == 1 && Array.isArray(xp[0])) { return tx.nil }

	// otherwise it's just the first element

	return xp[0]
}

// body method accesses the body of a xp, reversing simplifications

tx.body = xp => {

	if (! Array.isArray(xp)) { return undefined }

	// body of [] is []

	if (xp.length == 0) { return tx.nil }

	// a one-element xp is either an atomic head (body is nil) or list (body is the list)

	if (xp.length == 1) { 
		if (Array.isArray(xp[0])) { return xp[0] }

		return tx.nil 
	}

	// otherwise body is the second element

	let body = xp[1]

	// if the body has been reduced to an atom, reconstitute it as a singleton array
	if (tx.isAtomic(body)) { return [body] }
	
	// otherwise return the body
	
	return body
}

// tail method accesses the tail of a xp, reversing simplifications

tx.tail = xp => {
	if (! Array.isArray(xp)) { return undefined }

	// tail is nil if unspecified

	if (xp.length < 3) { return tx.nil }

	return xp[2]

}

// first method accesses the first element of a xp's body, undefined if nothing there

tx.first = xp => tx.body(xp)[0]

// last method accesses the last element of a xp's body, undefined if nothing there

tx.last = xp => tx.body(xp)[tx.body.length-1]

// detect things about a T-expression

tx.isString = xp => typeof(xp) == 'string'
tx.isNumber = xp => typeof(xp) == 'number'
tx.isBoolean = xp => xp === true || xp === false || xp === null || xp === undefined
tx.isAtomic = xp => tx.isString(xp) || tx.isNumber(xp) || tx.isBoolean(xp)





// because it's annoying to have the isArray function somewhere else

tx.isArray = xp => Array.isArray(xp)

// and we need a sensible definition of Object that excludes arrays

tx.isObjectlike = xp => typeof(xp) == 'object'
tx.isObject = xp =>  tx.isObjectlike && ! tx.isArray(xp)

// Javascript has no unique 'nil' object like Lisp, so any array is nil if it has length zero

tx.isNil = xp => tx.isArray(xp) && xp.length == 0

// a Singleton is a one-element array

tx.isSingleton = xp => tx.isArray(xp) && xp.length == 1

// a Form is a non-nil array (may be a List or a Term)

tx.isForm = xp => tx.isArray(xp) && xp.length > 0

// a T-expression is a Term if it has a non-nil head

tx.isTerm = xp => tx.isForm(xp) && ! tx.isNil(tx.head(xp))

// a T-expression is a List if it has a nil head

tx.isList = xp => tx.isForm(xp) && tx.isNil(tx.head(xp))


// a T-expression is Simple if it has a nil Tail

tx.isSimple = xp => tx.isArray(xp) &&	tx.isNil(tx.tail(xp))

// or is Extended if it has a non-nil Tail

tx.isExtended  = xp => tx.isArray(xp) && ! tx.isNil(tx.tail(xp))

// TODO: add checks for whether an extended xp only contains certain heads
// TODO: once we have whitespace parsing, add checks for is a list whitespace-complete only 


	
// A simple test framework for testing parsing and writing
	
tx.testPatternsRoundtrip = [
	["", 			[]					],
	["x",			[["x"]],			],
	["x y",			[["x","y"]]			],
	["`x",			["x"]				],
	["`x 1",		["x",1]				],
	["`x 1 2",		["x",[1,2]]			],
	["1",			[[1]]				],
	["[1] 2",		[[ [[1]], 2 ]]  	],
	["`[1] `2",		[ [[1]], [], [2]] 	],
	["`[1] `2 3",	[ [[1]], [], [2,3]]	]
]

tx.testPatternsParseOnly = [
//	["`", 			[]					],
//	["``", 			[]					],
//	["``a", 		["a"]				]
]

tx.testResult = r => r ? "pass" : "FAIL"

tx.test = () => 	tx.testPatternsRoundtrip.every(tx.testRoundtrip) && 
					tx.testPatternsParseOnly.every(tx.testParseOnly)

tx.testParseOnly = t => tx.testPair(t[0],t[1],true, false, true)
tx.testWriteOnly = t => tx.testPair(t[0],t[1],false,true, true)
tx.testRoundtrip = t => tx.testPair(t[0],t[1],true, true, true)


tx.testPair = (xp,ar, testparse, testwrite, loud) => {
	let arjs = JSON.stringify(ar)
		
	let xp2ar = JSON.stringify(tx.parse(xp))
	let ar2xp = tx.write(ar,true)
				
	let parseok = xp2ar == arjs
	let writeok = ar2xp == xp
	
	let parse = "testing: [" + xp + "] ==> " + JSON.stringify(ar)
	parse += " " + tx.testResult(parseok)
	if (! parseok) { parse += ": " + xp2ar }
						
	let write = "testing: [" + xp + "] <== " + JSON.stringify(ar)
	write += " " + tx.testResult(writeok)
	if (! writeok) { write += ": [" + ar2xp + "]" }
		
	if (loud && testparse) {console.log(parse) }
	if (loud && testwrite) {console.log(write) }
			
	return (parseok || ! testparse) && (writeok || ! testwrite)
}




