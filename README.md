# Tamarix is a term-expression parser for Javascript.
## What's a term-expression?

Term-expressions are a new, ultra-minimalist data representation framework designed for cross-platform storage and transfer of arbitrary tree-structured data. 

They are based on the combination of Prolog "term structure" with Lisp "S-expressions", giving a linked list of tagged arrays that can be cleanly serialised to and from Unicode in a simple text format.

Term-expressions are not a language - they are a simple grammar, along the lines of S-expressions, JSON or XML, on which multiple layers of languages can be built.

**For Javascript people**: A term-expression is a terser, cleaner alternative to JSON for serialising Javascript object and array structure, that can handle much more diverse types of data - including user-specified data unknown to the programmer at the time an application is built. Tamarix specifically is built around taking some Javacript arrays to be Tamarix "forms", but the body part of a Tamarix form is an ordinary Javascript array, so any existing Javascript array libraries should work fine on the body of an expression. There is also a special write mode which lets you export **any** Javascript array and read it in again, so you can use Tamarix exactly as a drop-in replacement for JSON parse and stringify without caring about its data model. Tamarix isn't yet a complete "pickling" solution for all possible Javascript object structure - it only does strings, numbers, logic values, objects and arrays so far - though it could potentially head in that direction.

**For Lisp or Scheme people**: Think of a term-expression as an S-expression extended (at the syntax level) with the ability for the CDR to be a uniquely distinguishable form, not just a list or atom; alternatively, think of it as a list represented natively as an array (with a type-tag) plus a CDR, rather than as pairs. This reduces some of the flexibility and fine-grained data sharing of "lists as pairs", but in exchange means you don't need a separate vector type to do parallelisation or to represent large chunks of storage, so it makes the Lisp data model better suited to low-level memory and storage management. Plus, you get something like "type tagging" (a very weak, flexible form of type tagging, with no specific type theory behind it, so interpretation of terms is up to the programmer to define). If you use Racket: you know that 'hash' syntax that it uses for everything that's not an atom or a list? Well, term-expressions are that hash syntax turned into a first-class thing, so every kind of expression is just natively made out of those. And running on arrays.

**For Prolog people** (all five of you): A term-expression is a term (functor and body) plus a Lisp-like CDR, so logical terms can be linked together into larger run-on structures that better parallel both machine memory layouts and natural language. This also means you don't need special syntax for lists - they just fall out naturally as another type of term. Term-expressions came from messing around with Prolog and asking if there couldn't be a better way of expressing term structure that would make it easier to use as a framework for making domain-specific languages and document markup. 

**For object-oriented people**: A term-expression is like a very weak form of an object, where each in-memory object has an exactly defined syntax representation, so you can load and store it from a text (or any other) file with no loss of data - but much weaker guarantees about its contents (or restrictions placed on its contents) than normal object systems. It can represent both parse trees and in-memory layouts, so everything from documentation to source code to object code to databases and network packets could be expressed in the one formalism. It is more general than objects in that it deliberately intends to capture program and data text, rather than just compiled program executable data.

**For Wiki, Markdown, Postscript and TeX people**: A term-expression is an ultra-minimal set of syntax (just three characters plus whitespace) designed to not conflict too much with either English, or with standard coding languages, so you could have one syntax that captures block-type structures found in both documentation and code. In structure, term-expressions very closely resemble TeX expressions, just with a well-defined mapping to data structures behind them, so they can be used for general semantic markup, not just typesetting. Also, they operate over lists of words rather than lists of characters; they do Lisp-style chunking into wordlike symbols first. I'm reasonably confident that that is a helpful thing to do even in the realm of document markup. 

**For TCL, Rebol and Red people**: You pretty much already get where I'm coming from. Tamarix is about trying to do Rebol-style blocks in JSON; but I have the same aspirations toward being a full-stack language that could extend up to high-level constructs and down to bare metal. The only difference is that term-expressions are like blocks with two added slots, 'head' and 'tail'. (And there's no actual language added over the top, yet.)

**For C or Forth people**: A term-expression could be as lightweight as a chunk of raw memory storage marked up with lengths, pointers and type indicators. The format should be reasonably well adaptable to diverse, Internet-scale collections of cache-friendly storage (at least more so than, say, the Lisp concept of lists constructed out of pairs). 

## Why Term-expressions?

* because XML is too noisy
* because JSON is too restricted
* because S-expressions are too ambiguous
* because everything else is too specialised

## What does Tamarix specifically do?

Tamarix is a Javascript testbed for the term-expression concept. It provides a parser and a writer to import and export from Unicode strings in the term-expression syntax, to term-expressions represented as Javascript arrays (and other Javascript objects including numbers, strings, objects and logic values).

The 'tx' object provides a number of functions for Javascript arrays representing term-expressions:

- `head` (the type-like part of the expression, which currently can be any expression)
- `body` (the array-like or list-like part of the expression)
- `tail` (the CDR-like part of the expression representing its continuation, either nil or another expression)
- `nil` (a Lisp-like single shared nil object)
- `isObjectlike` (is this expression a Javascript object type, eg, either object or array)
- `isObject` (is this expression specifically a Javascript object, not an array)
- `isArray` (is this expression a Javascript array)
- `isNil` (is this expression nil-like)
- `isString` (is this expression a Javascript string)
- `isNumber` (is this expression a Javascript number)
- `isBoolean` (is this expression a Javascript native logic value)
- `isAtomic` (is this expression a string, number or Boolean)
- `isForm` (is this expression an array that could represent a Term-expression form)
- `isList` (is this expression a form that is a List, ie, with nil head)
- `isTerm` (is this expression a form that isn't a list, ie a non-nil head)
- `isSimple` (is this expression a form that has a nil tail)
- `isExtended` (is this expression a form that has a non-nil tail)

- `parse` (read Unicode text as an expression, or throw an error if unparseable)
- `write` (write an expression to Uniciode text)

Both parse and write can take state settings that modify how they work. Write particularly can operate in 'strict' or 'literal' modes - in strict mode unprintable characters are escaped (using a string term-expression) while in literal mode all characters are added to the string, printable or not.

## Tamarix expressions look like this:
```
[hello I'm a list 1 2 3 [with sublists]]
```
or

```
[`some-term hello I'm a term 1 2 3 [with sublists] and [`another-term subterms]]
```

or even

```
[`some-term hello I'm a term 1 2 3 `another-term that ends in another term what is this I don't even]
```

or 

```
[`[`complicated-term a complicated term [1 2 3]] hello I'm a very complicated term 
`another-term that ends in another term why does this keep happening to me]
```

or
```
[`~~~
This Is Just To Say

I have parsed
the linebreaks
that were [in
your source code]

and which
you were probably
discarding
at the lexer]

[~w `[]
This Is Just To Say `~w `[]
`~w `[]
I have parsed `~w `[]
the linebreaks `~w `[]
that were [in `~w `[]
your source code] `~w `[]
`~w `[]
and which `~w `[]
you were probably `~w `[]
discarding `~w `[]
at the lexer]
```

or
```
["Great [``][`][[[[[[```` googly moogly[`][``] what insane kind of `quotation` did I just see??!!!" shrieked Tom, swiftly.]

[[`]the quotes mostly come at night[`] [`]     mostly     [`]]
```

## Quote sequences

To deal with the complexities caused by mixing natural language and coding languages, Tamarix doesn't use either single or double characters as quotation marks.

Instead, it uses a single backtick character (or a sequence of them) inside brackets to act as a quote sequence.

Using quote sequences rather than quotation characters means that quotations can contain any possible character sequences - including URLs, Windows path names, English text containing apostrophes or dialogue, or including other expressions that themselves contain quote sequences. Just extend the length of the outermost quote sequence if you need to quote something really tricky.

## Reserved heads

To get from 'just a tree of expressions' to 'Javascript object storage' and back again, there needs to be a little bit of 'special sauce' in the parser to represent Javascript objects.

Tamarix uses term heads starting with the tilde character (~) as reserved heads. Reserved heads include:

- `~s` for strings (only needed when writing or parsing strings that look like numbers or that contain unprintable characters - ~s terms are a series of strings and lists representng 'string escapes' - eg n for newline, t for tab, and u for unicode codepoints)

- `~n` for numbers (only needed when writing or parsing numeric values that look like strings, eg Infinity and NaN )

- `~o` for Javascript objects

- `~a` for Javascript arrays (only used when writing in 'Javascript' mode, which interprets arrays not as Tamarix terms but as raw arrays)

- `~true`, `~false`, `~null`, `~undefined` (for Javascript logic values)

- `~w` for parsed whitespace

- `~~~` for whitespace block literals

- `~q` for head quotations

Any of these reserved heads can be used by the programmer, but they'll be quoted with ~q when written out to text and dequoted when parsed back in

## Dot expressions

Currently Tamarix has no equivalent of the S-expression 'dot' operator, because it can get by without one. There are only two atomic value types which can be in tail place of an expression - string and number. And each of these has a term form - `~s` and `~n`. So to express the equivalent of Lisp's

```
(1 2 . 3)
```
you would write in Tamarix
```
[1 2 `~n 3]
```
and for the string equivalent
```
(a b . c)
```
you would write
```
[a b `~s c]
```
But if any language wanted to implement a Lisplike dot operator itself, it could use `. for this purpose, because Tamarix does not reserve any heads not beginning with tilde.


## Automatic cycle and duplicate value elimination

To come. Tamarix will be able to detect cycles and duplicate values, write them out to text and parse them back in again.

## Whitespace parsing

Because I would like term-expressions to be able to compete with Wiki notation and Markdown for making lightweight, slightly structured text documents with semantic content (Wikis are one of my core use cases), I've added a fairly experimental feature to Tamarix, but one I think is important: whitespace parsing.

The reserved head `~~~ indicates a block that should have its whitespace parsed literally, not ignored by the parser. Whitespace parsing will only last until the close-bracket that ends that block.

Parsed whitespace is represented in memory by terms with a special whitespace symbol as a head, and can be written out either literally or symbolically. In symbolic mode, each whitespace term has the ~w head.

It is up to the programmer to deal with whitespace sensibly in parsed term structure if they choose to use this feature. Whitespace will appear as the tail of an expression, rather than symbols within it, so it can be easily skipped over.

## Future extensions

The whole idea of Tamarix is that it shouldn't need too much extension because term-expressions should be a reasonably complete, lightweight, verifiable framework on which whole towers of languages, each with their own semantics, can be built.

However there are a couple of potentially missing pieces which might still be required to make that vision possible.

* Text editor or IDE support for Tamarix quote sequences: to come (probably in SciTE first).

* The reserved heads may be subject to change, and particularly the behaviour of numbers on non-Javascript platforms.

* A list pointer mechanism. Faking CDR operations over arrays using Javascript 'slice' is a very no good bad thing that nobody should ever do; it might be worthwhile to define instead a small object mechanism giving a pointer into an array (ie, a pair of array plus integer, and some methods for faking a length and retrieving elements), if anyone really wants to do Lisplike CDR over term-expressions. There are a number of these pointer libraries out in npm already, which I have not yet evaluated to see which would be best for Tamarix, or if I should write my own. Serialising a pointer to term-expression Unicode of course should be easy - a reserved head. There are probably extremely bad security implications for using such a mechanism and passing it around outside your own program without realising that it could give access to the entire array.

* More built-in operations on expressions. As I learn more about what operations are natural and ideomatic to use on term-expressions, I will probably add these into Tamarix. I'm thinking particularly about how to deal with Extended forms (ie with non-nil tails). 

* Pretty printing: At the moment the 'write' function takes two modes, a 'strict' mode which escapes unprintable characters, and a 'literal' mode which doesn't. It would probably make sense to add 'pretty printing' (eg adding indentation and newlines) of known term types into the write mechanism - but only when not in 'literal' mode. I may look into this for Javascript objects first.

* An object-oriented layer. Well. At the moment I'm trying to stay clear of OOP mechanisms because I don't think of them as well-defined in a platform-neutral way. So Tamarix is not especially object-oriented and is not trying to be. It's a bunch of functions that do things to arrays. If there does turn out to be a very good argument for building an OOP layer over top of those functions, and that argument doesn't come with exceptions and downsides, I'll think about adding that in.

* Some form of 'validated type tagging'. What that validation would mean is the problematic part (it requires defining a language or a theory, which is highly nontrivial and I don't want in the base syntax/protocol). But it would be useful to have some way of marking a head as being 'validated' in some defined-elsewhere sense, and then mark that tag as validated, or to-be-validated. This would catch us up to object-oriented platforms and databases (and on Javascript, would probably be implemented using an object prototype and immutability). The write syntax would probably only require allocating a reserved head.

## What is a Tamarix, anyway?

It is a hardy, desert-dwelling tree. https://en.wikipedia.org/wiki/Tamarix
